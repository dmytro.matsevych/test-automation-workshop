﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using TestStack.White;
using TestStack.White.InputDevices;
using TestStack.White.UIItems;
using TestStack.White.UIItems.Finders;
using TestStack.White.UIItems.WindowItems;

namespace Explanation.AutomationId.AutomationTest
{
    [TestClass]
    public class AutomationTests
    {
        private const string TEXT_BOX_CLASS_NAME = "TextBox";
        private const string TEXT_BOX3_NAME = "txtBoxFor_Field3";
        private const string TEXT_BOX4_AUTOMATION_ID = "textBox_for_field4";
        private const string MAIN_WINDOW_TITLE = "Explanations.AutomationId";
        private const string BUTTON_CANCEL_AUTOMATION_ID = "btnCancel";
        private const string appPath = @"../../../Explanations.AutomationId/bin/Debug/Explanations.AutomationId.exe";
        private Application app;
        private Window appWnd;

        [TestInitialize]
        public void RunApp()
        {
            app = Application.AttachOrLaunch(new System.Diagnostics.ProcessStartInfo(appPath));
            appWnd = app.GetWindow(MAIN_WINDOW_TITLE);
        }

        [TestMethod]
        public void GetTextBoxWithOutAutomationId()
        {
            // You have 2 TextBoxes with same set of identification data, so you can't identify them separately.
            // According to this we need to guess what is the necessary TextBox.
            // This approach is bad, because we don't exactly sure that we enter text in the required TextBox.
            // So, try to get all of them from Application Main Window:
            var txtBoxes = appWnd.GetMultiple(SearchCriteria.ByClassName(TEXT_BOX_CLASS_NAME));

            // Go through all of them and set some text for each of them.
            for (int i = 0; i < txtBoxes.Length; ++i)
            {
                txtBoxes[i].Enter(string.Format("Test automation text for TextBox: {0}", i));
            }
            // As you can see, after this loop we enter text in all of UIElement's with class name TextBox.

            // Small delay before next action.
            Thread.Sleep(2000);

            // Than you have TextBox with Name property.
            // If AutomationId property is not set, then AutomationId can be set with x:Name property.
            var field3TextBox = appWnd.Get<TextBox>(SearchCriteria.ByAutomationId(TEXT_BOX3_NAME));
            field3TextBox.Text = "Some new text for field 3";
            Thread.Sleep(2000);

            // But if AutomationId property is set manually than Name property won't be overwritten.
            var field4TextBox = appWnd.Get<TextBox>(SearchCriteria.ByAutomationId(TEXT_BOX4_AUTOMATION_ID));
            field4TextBox.Text = "Some new text for field 4";
            Thread.Sleep(2000);

            var btnCancel = appWnd.Get<Button>(SearchCriteria.ByAutomationId(BUTTON_CANCEL_AUTOMATION_ID));
            Mouse.Instance.Location = btnCancel.ClickablePoint;
            Thread.Sleep(2000);
            Mouse.Instance.Click();
            Thread.Sleep(2000);
        }

        [TestCleanup]
        public void CloseApp()
        {
            appWnd.Close();
            app.Close();

            appWnd = null;
            app = null;
        }
    }
}
